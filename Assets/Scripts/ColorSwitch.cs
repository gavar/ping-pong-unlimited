﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The scrip for material color animation.
/// </summary>
public class ColorSwitch : MonoBehaviour
{
	/// <summary>
	/// The animation curve for color animation.
	/// </summary>
	public AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, 2, 1);

	/// <summary>
	/// The material colors.
	/// </summary>
	public List<Color> colors = new List<Color> { Color.red, Color.green, Color.blue };

	protected int index = 0;
	protected float time = 0;
	protected Color fromColor;
	protected Color toColor;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	public void Start()
	{
		fromColor = renderer.material.color;
		toColor = colors[index];
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	public void Update()
	{
		// Update animation time
		time += Time.deltaTime;

		// Clamp time within aniamtion duration
		if (time > curve[curve.length - 1].time)
		{
			// Reset time
			time = curve[0].time;

			// Switch color
			index = (index + 1) % colors.Count;
			fromColor = renderer.material.color;
			toColor = colors[index];
		}

		// Update material color depending on animation time
		renderer.material.color = Color.Lerp(fromColor, toColor, curve.Evaluate(time));
	}
}
