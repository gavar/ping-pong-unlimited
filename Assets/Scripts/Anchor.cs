﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using System;
using UnityEngine;

/// <summary>
/// The component which place objects relativly to the screen. 
/// </summary>
[ExecuteInEditMode]
public class Anchor : MonoBehaviour
{
	/// <summary>
	/// The object position on the screen.
	/// </summary>
	public TextAnchor anchor = TextAnchor.MiddleCenter;

	/// <summary>
	/// The object offset.
	/// </summary>
	public Vector3 offset = Vector3.zero;

	/// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	public void OnEnable()
	{
		Reposition();
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	public void Update()
	{
#if UNITY_EDITOR
		Reposition();
#endif
	}

	/// <summary>
	/// Refresh the position of the object.
	/// </summary>
	public void Reposition()
	{
		var bounds = new Bounds(Vector3.zero, new Vector3(Screen.width, Screen.height));
		transform.localPosition = GetPosition(bounds, anchor) + offset;
	}

	/// <summary>
	/// Calculate position of the object within bounds.
	/// </summary>
	/// <param name="bounds">The bounds to keep object.</param>
	/// <param name="anchor">The object's anchor within bounds.</param>
	/// <returns>The poisition of the object within bounds.</returns>
	public static Vector3 GetPosition(Bounds bounds, TextAnchor anchor)
	{
		var center = bounds.center;
		var extents = bounds.extents;

		switch (anchor)
		{
			case TextAnchor.MiddleCenter:
				return center;

			case TextAnchor.UpperCenter:
				return new Vector3(center.x, extents.y, center.z);

			case TextAnchor.LowerCenter:
				return new Vector3(center.x, -extents.y, center.z);

			case TextAnchor.MiddleLeft:
				return new Vector3(-extents.x, center.y, center.z);

			case TextAnchor.MiddleRight:
				return new Vector3(extents.x, center.y, center.z);

			case TextAnchor.LowerLeft:
			case TextAnchor.LowerRight:
			case TextAnchor.UpperLeft:
			case TextAnchor.UpperRight:
			default: throw new NotImplementedException();
		}
	}
}
