﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using UnityEngine;

/// <summary>
/// Script with simple menu GUI.
/// </summary>
public class MenuWindow : MonoBehaviour
{
	protected static GUILayoutOption btnWidth;
	protected static GUILayoutOption btnHeight;
	protected static Vector2 size = new Vector2(250, 100);
	protected static float density;
	protected static int windowID = 0;
	protected static string windowName = "Menu";

	/// <summary>
	/// The key to call exit menu.
	/// </summary>
	public KeyCode key = KeyCode.Escape;

	/// <summary>
	/// Whether the window is currently displayed.
	/// </summary>
	public bool display;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	public void Awake()
	{
		density = Screen.dpi <= 0 ? 1 : Screen.dpi / 160;
		density *= Screen.height / 480f;
		btnWidth = GUILayout.MinWidth(100);
		btnHeight = GUILayout.MinHeight(20);
	}

	/// <summary>
	/// Display GUI.
	/// </summary>
	public void OnGUI()
	{
		if (!display) display = Event.current.keyCode == key;
		if (!display) return;

		var mCenter = new Vector2(Screen.width / 2f, Screen.height / 2f) / density;
		var mRect = new Rect(mCenter.x - size.x / 2f, mCenter.y - size.y / 2f, size.x, size.y);
		GUI.skin.button.fontSize = (int)(14);
		GUI.skin.window.fontSize = (int)(14);

		var matrix = GUI.matrix;
		matrix[0, 0] *= density;
		matrix[1, 1] *= density;
		matrix[2, 2] *= density;
		GUI.matrix = matrix;

		GUILayout.Window(windowID, mRect, DisplayWindow, windowName);
	}

	/// <summary>
	/// Window GUI function.
	/// </summary>
	/// <param name="id">The window ID.</param>
	private void DisplayWindow(int id)
	{
		GUILayout.Space(10);

		switch (id)
		{
			// Menu
			case 0:
				GUI.backgroundColor = Color.white;

				// Hide window
				GUI.backgroundColor = Color.green;
				if (GUILayout.Button("Continue", btnHeight))
					display = false;

				GUI.backgroundColor = Color.white;
				if (GUILayout.Button("Restart", btnHeight))
				{
					// Hide window
					display = false;
					// Place new ball
					BroadcastMessage("ReplaceBall");
				}

				GUI.backgroundColor = Color.red;
				if (GUILayout.Button("Quit", btnHeight))
				{
					// Display exit window
					windowID = 1;
					windowName = "Quit Game";
				}
				break;

			// Exit
			case 1:

				GUILayout.Label("Are you sure you want to quit?");
				GUILayout.FlexibleSpace();
				GUILayout.BeginHorizontal();

				GUI.backgroundColor = Color.red;
				if (GUILayout.Button("Yes", btnWidth, btnHeight))
					Application.Quit();

				GUILayout.FlexibleSpace();

				GUI.backgroundColor = Color.green;
				display &= !GUILayout.Button("No", btnWidth, btnHeight);

				if (!display) { windowID = 0; windowName = "Menu"; }
				GUILayout.EndHorizontal();
				break;
		}

		// Pause/Continue game
		Time.timeScale = display ? 0f : 1f;

		// Restore color
		GUI.backgroundColor = Color.white;
	}
}
