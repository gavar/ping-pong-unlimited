﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using UnityEngine;

/// <summary>
/// The component for bouncing ball.
/// </summary>
public class Ball : MonoBehaviour
{
	protected Vector3 startVelocity;

	/// <summary>
	/// The current velocity of the ball.
	/// </summary>
	public Vector3 velocity;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	public void Start()
	{
		startVelocity = velocity;
		rigidbody.AddForce(velocity, ForceMode.Impulse);
	}

	/// <summary>
	/// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
	/// </summary>
	public void FixedUpdate()
	{
		// Update current velocity
		velocity = rigidbody.velocity;

		// Fix slow velocity
		if (Mathf.Abs(velocity.x) < 50)
			velocity.x += velocity.x > 0 ? 200 : -200;

		if (Mathf.Abs(velocity.y) < 50)
			velocity.y += velocity.y > 0 ? 200 : -200;

		// Set calculated velocity
		rigidbody.velocity = velocity;
	}

	/// <summary>
	/// OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider.
	/// </summary>
	/// <param name="info">The collision info.</param>
	public void OnCollisionEnter(Collision info)
	{
		// Get ball collision point
		var hit = info.contacts[0];

		// Display last ball direction and hit normal
		Debug.DrawLine(hit.point, hit.point - velocity, Color.red, 1f);
		Debug.DrawLine(hit.point, hit.point + hit.normal * 100, Color.blue, 1f);

		// Bounce
		velocity = Vector3.Reflect(velocity, hit.normal);

		// Display new ball direction
		Debug.DrawLine(hit.point, hit.point + velocity, Color.green, 1f);

		// Fix slow velocity
		if (Mathf.Abs(velocity.x) < 50)
			velocity.x += velocity.x > 0 ? 200 : -200;

		if (Mathf.Abs(velocity.y) < 50)
			velocity.y += velocity.y > 0 ? 200 : -200;

		// Set calculated velocity
		rigidbody.velocity = velocity;
	}

	/// <summary>
	/// OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider.
	/// </summary>
	/// <param name="info">The collision info.</param>
	public void OnCollisionStay(Collision info)
	{
		// Check whether ball has stuck
		if (rigidbody.velocity.magnitude > 1) return;

		// Get ball collision point
		var hit = info.contacts[0];

		// Bounce
		velocity = Vector3.Reflect(velocity, hit.normal);

		// Get closest point to bounds considering last velocity
		var point = info.collider.ClosestPointOnBounds(hit.point + velocity);

		// Move ball from the given point in opposite direction
		var pos = point + Vector3.Scale(-velocity.normalized, collider.bounds.extents);
		rigidbody.MovePosition(pos);
	}

	/// <summary>
	/// OnCollisionExit is called when this collider/rigidbody has stopped touching another rigidbody/collider.
	/// </summary>
	/// <param name="info">The collision info.</param>
	public void OnCollisionExit(Collision info)
	{
		// Avoid getting ball out of screen
		KeepWithinScreen();
	}

	/// <summary>
	/// Place ball on center of the screen.
	/// </summary>
	public void ReplaceBall()
	{
		// Place on center of screen
		rigidbody.position = new Vector3(0, 0, rigidbody.position.z);

		// Add random angle offset
		velocity = Quaternion.AngleAxis(Random.Range(-180, 180), Vector3.forward) * startVelocity;

		// Set new velocity
		rigidbody.velocity = velocity;
	}

	/// <summary>
	/// Keep ball within screen.
	/// </summary>
	protected void KeepWithinScreen()
	{
		var bounds = new Bounds(Vector3.zero, new Vector3(Screen.width, Screen.height));
		bounds.size -= collider.bounds.size;
		var pos = Racket.WithinBounds(bounds, rigidbody.position);
		pos.z = transform.position.z;
		rigidbody.MovePosition(pos);
	}
}
