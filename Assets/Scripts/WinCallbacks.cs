﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using UnityEngine;

/// <summary>
/// Script for Windows callbacks tracing.
/// </summary>
public class WinCallbacks : MonoBehaviour
{

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	public void Start()
	{
		// Subscribe for screen changes
		UnityEngine.WSA.Application.windowSizeChanged += OnWindowSizeChanged;

		// Display welcome messaage
		ToastHelper.DisplayToast("Welcome to the Ping-Pong Unlimited!");
	}

	/// <summary>
	/// Callback for window size changes.
	/// </summary>
	/// <param name="width">The new width of the screen.</param>
	/// <param name="height">The new height of the screen.</param>
	public void OnWindowSizeChanged(int width, int height)
	{
		// Display toast messaage
		ToastHelper.DisplayToast("New Window Size", "X = " + width + " Y = " + height);

		// Update position of nested elements
		BroadcastMessage("Resize");
		BroadcastMessage("Reposition");
		BroadcastMessage("ReplaceBall");
	}

	/// <summary>
	/// Sent to all game objects before the application is quit.
	/// </summary>
	public void OnApplicationQuit()
	{
		// Display welcome messaage
		ToastHelper.DisplayToast("Thanks for play!", "See you later ;)");
	}
}
