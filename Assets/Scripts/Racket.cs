﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using UnityEngine;

/// <summary>
/// The script for racket movement.
/// </summary>
public class Racket : MonoBehaviour
{
	/// <summary>
	/// The scope of the racket movement.
	/// </summary>
	public Collider scope;

	/// <summary>
	/// Whether to block X-axis movement.
	/// </summary>
	public bool axisX = true;

	/// <summary>
	/// Whether to block Y-axis movement.
	/// </summary>
	public bool axisY = true;

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	public void Update()
	{
		// Move racket depening on mouse position
		var mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		transform.position = FreezePosition(mouse);
	}

	/// <summary>
	/// LateUpdate is called every frame, if the Behaviour is enabled.
	/// </summary>
	public void LateUpdate()
	{
		// Keep racket within scope
		var bounds = new Bounds(scope.bounds.center, scope.bounds.size - collider.bounds.size);
		transform.position = WithinBounds(bounds, transform.position);
	}

	/// <summary>
	/// Lock the movement position depending on <see cref="axisX"/> and <see cref="axisY"/> flags.
	/// </summary>
	/// <param name="position">The position of the object.</param>
	/// <returns>The position of the object with frozen axes.</returns>
	protected Vector3 FreezePosition(Vector3 position)
	{
		var pos = transform.position;
		if (!axisX) position.x = pos.x;
		if (!axisY) position.y = pos.y;
		position.z = pos.z;
		return position;
	}

	/// <summary>
	/// Clamp position within bounds.
	/// </summary>
	/// <param name="bounds">The source bounds.</param>
	/// <param name="pos">The position to keep within bounds.</param>
	/// <returns>The position within bounds.</returns>
	public static Vector3 WithinBounds(Bounds bounds, Vector3 pos)
	{
		if (!bounds.Contains(pos))
		{
			var mMin = bounds.min;
			var mMax = bounds.max;
			for (int i = 0; i < 3; i++)
				pos[i] = Mathf.Clamp(pos[i], mMin[i], mMax[i]);
		}
		return pos;
	}

	/// <summary>
	/// OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider.
	/// </summary>
	/// <param name="info">The collision info.</param>
	public void OnCollisionEnter(Collision info)
	{
		// Process only collision with ball
		if (info.transform.name != "Ball") return;

		// Get ball velocity
		var velocity = info.rigidbody.velocity;

		// Add random angle offset
		velocity = Quaternion.AngleAxis(Random.Range(-10, 10), Vector3.forward) * velocity;

		// Speed up
		velocity *= 1.01f;

		// Set ball velocity
		info.rigidbody.velocity = velocity;
	}
}
