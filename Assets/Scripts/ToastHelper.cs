﻿#region Copyright
// -----------------------------------------------------------------------
//                           Ping-Pong Unlimited                          
//                     Copyright © 2013 Max Stankevich                    
//                          The MIT License (MIT)                         
// -----------------------------------------------------------------------
// Contacts:                                                              
// E-mail:   mailto:m.stankevicsh@gmail.com                               
// LinkedIn: http://lv.linkedin.com/pub/maksims-stankevics/53/351/765/    
// ----------------------------------------------------------------------- 
#endregion
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.WSA;

/// <summary>
/// The helper class for Windows toasts.
/// </summary>
public class ToastHelper
{
	/// <summary>
	/// Display the text toast.
	/// </summary>
	/// <param name="texts">The text to display with maximum 4 entries.</param>
	public static void DisplayToast(params string[] texts)
	{
		// Get template
		var length = Mathf.Min(texts.Length, 4);
		var template = Toast.GetTemplate(GetTextTemplate(length));
		if (string.IsNullOrEmpty(template)) return;

		// Load xml template
		var document = XDocument.Parse(template);

		// Fin text entries
		var toastElement = document.Element("toast");
		var visualElement = toastElement.Element("visual");
		var bindingElement = visualElement.Element("binding");
		var textElements = bindingElement.Elements("text").ToArray();

		// Set text for each entry
		for (int i = 0; i < length; i++) textElements[i].SetValue(texts[i]);

		// Display toast
		Toast.Create(document.ToString()).Show();
	}

	/// <summary>
	/// Get text toast template depending on text entries count.
	/// </summary>
	/// <param name="count">The number of text entries.</param>
	/// <returns>The toast template type.</returns>
	protected static ToastTemplate GetTextTemplate(int count)
	{
		if (count <= 1) return ToastTemplate.ToastText01;
		if (count <= 2) return ToastTemplate.ToastText02;
		if (count <= 3) return ToastTemplate.ToastText03;
		return ToastTemplate.ToastText04;
	}
}
